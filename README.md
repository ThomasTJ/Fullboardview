Kanboard Plugin for notes
==========================

This plugin adds a view for all boards on 1 page.

Plugin for https://github.com/fguillot/kanboard

NOT MAINTAINED
==============

The plugin is not maintained anymore before kanboard pull 2474 is cleared. Please see the official pull 2474 from stinnux at https://github.com/kanboard/kanboard/pull/2474 .
The future of this pull will be defined this plugins life.

Author
------

- TTJ
- License MIT

Installation
------------

- Decompress the archive in the `plugins` folder

or

- Create a folder **plugins/Fullboardview**
- Copy all files under this directory

or

- Clone folder with git

Use
---

As a projectmanager for all projects in Kanboard, I need 1 place to view all tasks to get an overview.

Features
--------

- View all boards on 1 page
- Move tasks between columns within the project (please see the section for bugs)
- Click on project title to get to the project

Todo
----

- Reduce the loading time

Other
-----

- Please be advised: ALOT of the code is based (copy'n'pasted) from the original Kanboard - only variables is changes. 
- The design is specialized with using the css code from here: https://gitlab.com/ThomasTJ/Kanboard_styleCSS
- The design also uses Themeplus - please see: https://github.com/phsteffen/kanboard-themeplus

Security issues and bugs
------------------------

- Project selector in top right is not redirecting to project. Variable is 0.
- Moving tasks from column to column makes the loading icon to stay - but the task is moved
- It is not possible to move task from project to project
- No fault procedures implemented

Tested on
---------

- Application version: 1.0.30
- PHP version: 5.5.9-1ubuntu4.17
- PHP SAPI: apache2handler
- OS version: Linux 3.13.0-74-generic
- Database driver: sqlite
- Database version: 3.8.2
