<?php

namespace Kanboard\Plugin\Fullboardview\Controller;

use Kanboard\Core\Base;

use Kanboard\Controller\BaseController;
use Kanboard\Controller\TaskModificationController;

use Kanboard\Core\Controller\AccessForbiddenException;
use Kanboard\Formatter\BoardFormatter;

class FullboardviewController extends BaseController
{

const TABLE = 'projects';
const TABLEprojecthasusers = 'project_has_users';

    public function allboardGetProjectnames()
    {

	// Get projectnames for dropdown in header
	$projectDrop = $this->db
		->table(self::TABLE)
		->columns(
			self::TABLE.'.name'
		)
		->asc('name')
		->findAll();

	return $projectDrop;

    }

    public function allprojectIdForFullView()
    {
        // Get id's for projects

        $user = $this->getUser();

	// For admin
	if ($this->userSession->isAdmin() ) {
        $projectIdForFullView = $this->db
                ->table(self::TABLE)
                ->columns(
                        self::TABLE.'.id',
                        self::TABLE.'.name'
                )
                ->asc('name')
                ->findAll();
	return $projectIdForFullView;
	}else{
	// Check project access
	$projectIdForFullView = $this->db
                ->table(self::TABLE)
                ->columns(
                        self::TABLE.'.id',
                        self::TABLE.'.name'
                )
		->left(self::TABLEprojecthasusers, 'tblProU', 'project_id', self::TABLE, 'id')
		->eq('tblProU.user_id', $user['id'])
                ->asc('name')
                ->findAll();
        return $projectIdForFullView;

	}

	// No failure procedure implemented if no project is assigned

    }


    public function allboardcollapsed()
    {

	// Get projectnames from function
	$projectDrop = $this->allboardGetProjectnames();
	$projectDrop = array_map("unserialize", array_unique(array_map("serialize", $projectDrop)));
        $uids = array();
        foreach($projectDrop as $r) $uids[] = $r['name'];

	// Get porject id and name from function
	$projectIdForFullView = $this->allprojectIdForFullView();
        $projectIdForFullView = array_map("unserialize", array_unique(array_map("serialize", $projectIdForFullView)));
        $uids2 = array();
        foreach($projectIdForFullView as $q) $uids2[] = $q['id'];

	// Special variables
	$countCSS = "1"; // Counter to ensure only 1 header
	$boardColExp = "col"; // Make board collapsed

	// Run swimlane rendering for all projects
	foreach ($uids2 as $i) {
        //$project = $this->getProject();
	$project['id'] = $i; // Change project id to array
        $search = $this->helper->projectHeader->getSearchQuery($project);

	$projectName = $this->db
                ->table(self::TABLE)
                ->columns(
                        self::TABLE.'.name'
                )
                ->eq(self::TABLE.'.id', $i)
                ->findOne();

        $this->response->html($this->helper->layout->app('Fullboardview:board/view_private', array(
	    'countCSS' => $countCSS,
	    'projectDrop' => $uids,
	    'boardColExp' => $boardColExp,
            'project' => $project,
            'title' => $projectName, //$project['name'],
            'description' => $this->helper->projectHeader->getDescription($project),
            'board_private_refresh_interval' => $this->configModel->get('board_private_refresh_interval'),
            'board_highlight_period' => $this->configModel->get('board_highlight_period'),
            'swimlanes' => $this->taskLexer
                ->build($search)
                ->format(BoardFormatter::getInstance($this->container)->withProjectId($project['id']))
        )));

	$countCSS = "2"; // Counter to ensure only 1 header

	}
    }


    public function allboardexpanded()
    {

        // Get projectnames from function
        $projectDrop = $this->allboardGetProjectnames();
        $projectDrop = array_map("unserialize", array_unique(array_map("serialize", $projectDrop)));
        $uids = array();
        foreach($projectDrop as $r) $uids[] = $r['name'];

        // Get project id and name from function
        $projectIdForFullView = $this->allprojectIdForFullView();
        $projectIdForFullView = array_map("unserialize", array_unique(array_map("serialize", $projectIdForFullView)));
        $uids2 = array();
        foreach($projectIdForFullView as $q) $uids2[] = $q['id'];

        // Special variables
        $countCSS = "1"; // Counter to ensure only 1 header
        $boardColExp = "exp"; // Make board collapsed

        // Run swimlane rendering for all projects
        foreach ($uids2 as $i) {
        //$project = $this->getProject();
        $project['id'] = $i; // Change project id to array
        $search = $this->helper->projectHeader->getSearchQuery($project);

        $projectName = $this->db
                ->table(self::TABLE)
                ->columns(
                        self::TABLE.'.name'
                )
                ->eq(self::TABLE.'.id', $i)
                ->findOne();

        $this->response->html($this->helper->layout->app('Fullboardview:board/view_private', array(
                'countCSS' => $countCSS,
                'projectDrop' => $uids,
                'boardColExp' => $boardColExp,
            'project' => $project,
            'title' => $projectName, //$project['name'],
            'description' => $this->helper->projectHeader->getDescription($project),
            'board_private_refresh_interval' => $this->configModel->get('board_private_refresh_interval'),
            'board_highlight_period' => $this->configModel->get('board_highlight_period'),
            'swimlanes' => $this->taskLexer
                ->build($search)
                ->format(BoardFormatter::getInstance($this->container)->withProjectId($project['id']))
        )));
        $countCSS = "2"; // Counter to ensure only 1 header

        }

     }

}
